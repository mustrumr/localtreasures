<?php
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Geocoder\Provider\GeocoderServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider;

$app = new Silex\Application();
$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
    'twig.options' => array('debug' => true),
    ));
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
    ));
$app->register(new Provider\DoctrineServiceProvider(), array('db.options' => array(
    'driver'   => 'pdo_pgsql',
    'dbname' => 'derduer',
    'host' => 'localhost',
    'user' => 'derduer',
    'password' => 'giga-2013-gøy',
    )));
$app->register(new GeocoderServiceProvider());

$app['debug'] = true;

$app->before(function () use ($app) {
    $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('layout.twig'));
});

/*Debug til twig
$app->get('/', function () use ($app) {
    return $app['twig']->render('index.twig', array('app' => $app));
});*/

/*Uten debug til twig*/
$app->match('/', function () use ($app) {
    return $app['twig']->render('index.twig');
});

$app->get('/hello', function() {
    return 'Hello!';
});

$app->match('/register', function (Request $request) use ($app) {
    // some default data for when the form is displayed the first time
    $data = array(
        'name' => 'Your name',
        'password' => 'Password',
        );

    $form = $app['form.factory']->createBuilder('form', $data)
    ->add('name')
    ->add('password')
    ->getForm();

    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $post = $app['db']->insert('users', array('name' => $data['name'], 'password' => $data['password']));
            return $app->redirect('/welcome');
        }
    }

    // display the form
    return $app['twig']->render('register.twig', array('form' => $form->createView()));
});

$app->match('/newfact', function (Request $request) use ($app) {
    $sql = "SELECT id, name FROM categories ORDER BY name";
    $categories = $app['db']->fetchAll($sql);
    $form_categories = array();
    foreach ($categories as $category) {
        $form_categories[$category["id"]] = $category["name"];
    }
    $data = array(
        'category' => 'Select category',
        'description' => 'Descriptionnnn',
        'location' => 'Location',
        );
    $form = $app['form.factory']->createBuilder('form')
    ->add('category', 'choice', array( 'choices'   => $form_categories ))
    ->add('description', 'text')
    ->add('location')
    ->getForm();

    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $post = $app['db']->insert('facts', 
                array('location' => $data['location'],
                    'description' => $data['description'],
                    'category_id' => $data['category'],
                    )
                );
            return $app->redirect('/thanks');
        }
    }
    return $app['twig']->render('newfact.twig', array('form' => $form->createView()));
});

$app->get('/user/{id}', function ($id) use ($app) {
    $sql = "SELECT * FROM users WHERE id = ?";
    $post = $app['db']->fetchAssoc($sql, array((int) $id));

    return  "<h1>{$post['id']}{$post['name']}</h1>";
});

$app->get('/fact/{id}', function ($id) use ($app) {
    $sql = "SELECT * FROM facts WHERE id = ?";
    $post = $app['db']->fetchAssoc($sql, array((int) $id));

    return  "<h1>{$post['id']} {$post['description']}</h1>";
});

$app->get('/listfacts', function () use ($app) {
    $geocoder = $app['geocoder'];
    $adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter();
    //$googleprovider = $geocoder->registerProvider( new \Geocoder\Provider\GoogleMapsProvider( $adapter, "Europe", false ));
    $providers = array(
            //new \Geocoder\Provider\BingMapsProvider($adapter, $app['BINGMAPS_API_KEY']),
            //new \Geocoder\Provider\CloudMadeProvider($adapter, $app['CLOUDMADE_API_KEY']),
            new \Geocoder\Provider\GoogleMapsProvider($adapter),
            new \Geocoder\Provider\OpenStreetMapsProvider($adapter),
            //new \Geocoder\Provider\YahooProvider($adapter, $app['YAHOO_API_KEY']),
            //new \Geocoder\Provider\GeocoderCaProvider($adapter),
            //new \Geocoder\Provider\GeocoderUsProvider($adapter),
            new \Geocoder\Provider\MapQuestProvider($adapter),
            //new \Geocoder\Provider\OIORestProvider($adapter),
            //new \Geocoder\Provider\IGNOpenLSProvider($adapter)
        );
    //$view_facts = array();
    //$sql = "select * from facts order by description";
    //$facts = $app['db']->fetchAll($sql);
    $geocoder->registerProviders($providers);
     foreach (array_keys($geocoder->getProviders()) as $provider) {
        $geocoder->using($provider);
        if ($provider != "free_geo_ip") {
            $result[$provider] = $geocoder->geocode('10 rue Gambetta, Paris, France')->toArray();
        }
    }
    return $app['twig']->render('listfacts.twig', array());
    /*$providers = $geocoder->getProviders( );
    //$providers = array_pop( $providers );
    var_dump($providers);
    $geocoder->using( $providers["google_maps"] );
    $result = $geocoder->geocode('10 rue Gambetta, Paris, France');
    return $result;*/
});

//Fetch categories and create form select
$app->get('/categories', function () use ($app) {
    $sql = "SELECT * FROM categories ORDER BY name";
    $categories = $app['db']->fetchAll($sql);
    foreach ($categories as $category) {
        $categories .= "ja";
    }

    return  "<pre>".var_dump($categories)."</pre>";
});

$app->get('/thanks', function() {
    return 'Many thanks for your contribution!';
});

$app->get('/factnear', function() use ($app) {
    //return 'Hello!';
    return $app['twig']->render('nearest.twig', array());
});

$app->get('/getfacts', function() use ($app) {
    $sql = "SELECT * FROM facts";
    $facts = $app['db']->fetchAll($sql);
    return $app->json($facts[0]);
});


$app->run();
